
--
-- Table structure for table `authassignment`
--

CREATE TABLE `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '2', NULL, 'N;'),
('admin', '3', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

CREATE TABLE `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, '', NULL, 'N;'),
('adminApp', 0, '', NULL, 'N;'),
('AppManagement', 1, 'Manage App', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

CREATE TABLE `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `id` int(11) NOT NULL,
  `attending` tinytext NOT NULL,
  `session_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `cost` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `note` text,
  `status` int(11) NOT NULL COMMENT 'Status : INT \n\n(1 - 2 - 3) (Not yet started - on progress - finished)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_type`
--

CREATE TABLE `tbl_course_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_damascus_list`
--

CREATE TABLE `tbl_damascus_list` (
  `id` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_damascus_list_entry`
--

CREATE TABLE `tbl_damascus_list_entry` (
  `id` int(11) NOT NULL,
  `damascus_list_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_icdl_card`
--

CREATE TABLE `tbl_icdl_card` (
  `id` int(11) NOT NULL,
  `first_name_en` varchar(255) NOT NULL,
  `last_name_en` varchar(255) NOT NULL,
  `un_code` varchar(10) NOT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL,
  `lang` tinyint(4) NOT NULL COMMENT '(English - Arabic)',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'For later use',
  `student_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_icdl_ticket`
--

CREATE TABLE `tbl_icdl_ticket` (
  `id` int(11) NOT NULL,
  `exam_type` tinyint(4) NOT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `icdl_card_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_instructor`
--

CREATE TABLE `tbl_instructor` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `added_date` datetime NOT NULL,
  `note` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` date NOT NULL,
  `num` int(11) NOT NULL,
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phone_number`
--

CREATE TABLE `tbl_phone_number` (
  `id` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `type` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `course_id` int(11) NOT NULL,
  `DOW` tinyint(4) DEFAULT NULL,
  `num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `residency` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `national_no` varchar(45) DEFAULT NULL,
  `img_src` varchar(255) DEFAULT NULL,
  `how_to_know_us` tinytext,
  `DOB` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_course_assignment`
--

CREATE TABLE `tbl_student_course_assignment` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `grade` tinyint(4) DEFAULT NULL COMMENT '(1 - 2 - 3)\n\n1 Excellent\n2 Very Good\n3 Good',
  `mark` tinyint(4) DEFAULT NULL,
  `discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_course_type_assignment`
--

CREATE TABLE `tbl_student_course_type_assignment` (
  `student_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'Status : INT\n1 - 2 - 3 (wait - ignor - retreat)\n\n',
  `registration_method` tinyint(4) NOT NULL DEFAULT '0',
  `initial_payment_num` int(11) DEFAULT NULL,
  `initial_payment` int(11) DEFAULT NULL,
  `preferred_time` varchar(255) DEFAULT NULL,
  `registration_date` date NOT NULL,
  `note` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `email`, `password`) VALUES
(2, 'Borhan Otuor', 'burhan@icard-sy.com', 'e056bb917fbb0b31a72aff5e4cae670796f2ca40'),
(3, 'Hasan Haj Hasan', 'hasan@icard-sy.com', 'faf3e35b662783bda3797874b1aff926b55320b1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD PRIMARY KEY (`itemname`,`userid`);

--
-- Indexes for table `authitem`
--
ALTER TABLE `authitem`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_attendance_rbl_session1_idx` (`session_id`),
  ADD KEY `fk_tbl_attendance_tbl_student1_idx` (`student_id`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_course_tbl_course_type1_idx` (`course_type_id`),
  ADD KEY `fk_tbl_course_tbl_instructor1_idx` (`instructor_id`);

--
-- Indexes for table `tbl_course_type`
--
ALTER TABLE `tbl_course_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_damascus_list`
--
ALTER TABLE `tbl_damascus_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_damascus_list_entry`
--
ALTER TABLE `tbl_damascus_list_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_damascus_list_entry_tbl_damascus_list1_idx` (`damascus_list_id`),
  ADD KEY `fk_damascus_list_entry_tbl_course1_idx` (`course_id`),
  ADD KEY `fk_damascus_list_entry_tbl_student1_idx` (`student_id`);

--
-- Indexes for table `tbl_icdl_card`
--
ALTER TABLE `tbl_icdl_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id_idx` (`student_id`);

--
-- Indexes for table `tbl_icdl_ticket`
--
ALTER TABLE `tbl_icdl_ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_icdl_card_id_tbl_icdl_card_idx` (`icdl_card_id`);

--
-- Indexes for table `tbl_instructor`
--
ALTER TABLE `tbl_instructor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tbl_payment_tbl_student1_idx` (`student_id`),
  ADD KEY `fk_tbl_payment_tbl_course1_idx` (`course_id`);

--
-- Indexes for table `tbl_phone_number`
--
ALTER TABLE `tbl_phone_number`
  ADD PRIMARY KEY (`id`,`owner_id`),
  ADD KEY `fk_tbl_phone_number_tbl_student1_idx` (`owner_id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rbl_session_tbl_course1_idx` (`course_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_course_assignment`
--
ALTER TABLE `tbl_student_course_assignment`
  ADD PRIMARY KEY (`student_id`,`course_id`),
  ADD KEY `fk_tbl_student_course_assignment_tbl_student1_idx` (`student_id`),
  ADD KEY `fk_tbl_student_course_assignment_tbl_course1_idx` (`course_id`);

--
-- Indexes for table `tbl_student_course_type_assignment`
--
ALTER TABLE `tbl_student_course_type_assignment`
  ADD PRIMARY KEY (`student_id`,`course_type_id`),
  ADD KEY `fk_tbl_student_course_type_assignment_tbl_course_type1_idx` (`course_type_id`),
  ADD KEY `fk_tbl_student_course_type_assignment_tbl_student1_idx` (`student_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_course_type`
--
ALTER TABLE `tbl_course_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_damascus_list`
--
ALTER TABLE `tbl_damascus_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_damascus_list_entry`
--
ALTER TABLE `tbl_damascus_list_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_icdl_card`
--
ALTER TABLE `tbl_icdl_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_icdl_ticket`
--
ALTER TABLE `tbl_icdl_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_instructor`
--
ALTER TABLE `tbl_instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_phone_number`
--
ALTER TABLE `tbl_phone_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD CONSTRAINT `fk_tbl_attendance_tbl_session1` FOREIGN KEY (`session_id`) REFERENCES `tbl_session` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_attendance_tbl_student1` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD CONSTRAINT `fk_tbl_course_tbl_course_type1` FOREIGN KEY (`course_type_id`) REFERENCES `tbl_course_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_course_tbl_instructor1` FOREIGN KEY (`instructor_id`) REFERENCES `tbl_instructor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_damascus_list_entry`
--
ALTER TABLE `tbl_damascus_list_entry`
  ADD CONSTRAINT `fk_damascus_list_entry_tbl_course1` FOREIGN KEY (`course_id`) REFERENCES `tbl_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_damascus_list_entry_tbl_damascus_list1` FOREIGN KEY (`damascus_list_id`) REFERENCES `tbl_damascus_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_damascus_list_entry_tbl_student1` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_icdl_card`
--
ALTER TABLE `tbl_icdl_card`
  ADD CONSTRAINT `student_id_f_key_student` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD CONSTRAINT `fk_tbl_payment_tbl_course1` FOREIGN KEY (`course_id`) REFERENCES `tbl_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_payment_tbl_student` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_phone_number`
--
ALTER TABLE `tbl_phone_number`
  ADD CONSTRAINT `fk_tbl_phone_number_tbl_student1` FOREIGN KEY (`owner_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD CONSTRAINT `fk_rbl_session_tbl_course1` FOREIGN KEY (`course_id`) REFERENCES `tbl_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_student_course_assignment`
--
ALTER TABLE `tbl_student_course_assignment`
  ADD CONSTRAINT `fk_tbl_student_course_assignment_tbl_course1` FOREIGN KEY (`course_id`) REFERENCES `tbl_course` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_student_course_assignment_tbl_student1` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_student_course_type_assignment`
--
ALTER TABLE `tbl_student_course_type_assignment`
  ADD CONSTRAINT `fk_tbl_student_course_type_assignment_tbl_course_type1` FOREIGN KEY (`course_type_id`) REFERENCES `tbl_course_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_student_course_type_assignment_tbl_student1` FOREIGN KEY (`student_id`) REFERENCES `tbl_student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
