<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="sidebar-container">
        <h3>خيارات</h3>
        <?php $this->widget('application.components.OptionsMenu', array(
            'options' => $this->options,
        )); ?>
    </div>
    <div class="section-container section-container-padding" style="background: transparent;">
        <div class="row">

            <div class="col-sm-9 col-md-9 col-sm-offset-3 col-md-offset-3">
                <h2 class="SubHeader"><?php echo $this->pageTitle; ?></h2>
                <?php echo $content; ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
<?php $this->endContent(); ?>